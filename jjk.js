/* Create a function which returns the number of true values there are in an array.

Examples
countTrue([true, false, false, true, false]) ➞ 2

countTrue([false, false, false, false]) ➞ 0

countTrue([]) ➞ 0
Notes
Return 0 if given an empty array.
All array items are of the type bool (true or false).  */

function countTrue(arr) {
	return arr.filter(val=>val===true).length
}

/* Create a function that takes an array of numbers and return "Boom!" 
if the digit 7 appears in the array. Otherwise, return "there is no 7 in the array".
sevenBoom([1, 2, 3, 4, 5, 6, 7]) ➞ "Boom!"
  /*7 contains the number seven.

sevenBoom([8, 6, 33, 100]) ➞ "there is no 7 in the array"
  None of the items contain 7 within them.

sevenBoom([2, 55, 60, 97, 86]) ➞ "Boom!"
  97 contains the number seven.*/

function sevenBoom(arr) {
  var result = "there is no 7 in the array"
  for (const el of arr) {
    for (const item of el.toString()) {
      if (item === "7") {
        result = "Boom!"
      }
    }
  }
  return result
}


/* Given a number, n, return a function which adds n to the number passed to it.
Examples
add(10)(20) ➞ 30

add(0)(20) ➞ 20

add(-30)(80) ➞ 50
Notes
All numbers used in the tests will be integers (whole numbers).
Returning a function from a function is a key part of understanding higher order functions (functions which operate on and return functions).*/

function add(n) {
	return function(x){
		return n+x;
	};
}

/* Write a function redundant that takes in a string str and returns a function that returns str.

Examples
const f1 = redundant("apple")
f1() ➞ "apple"

const f2 = redundant("pear")
f2() ➞ "pear"

const f3 = redundant("")
f3() ➞ ""
Notes
Your function should return a function, not a string. */

function redundant(str) {
	return function(){
		return str;
	};
}

/*create a simple JavaScript function that takes an array containing only numbers and returns the 
first element of the array. Here's how you can do it:*/

function getFirstElement(arr) {
  if (arr.length === 0) {
  }
  return arr[0];
}

/* A boomerang is a V-shaped sequence that is either upright or upside down. Specifically, a boomerang can be defined as: sub-array of length 3, with the first and last digits being the same and the middle digit being different.

Some boomerang examples:

[3, 7, 3], [1, -1, 1], [5, 6, 5]
Create a function that returns the total number of boomerangs in an array.

To illustrate:

[3, 7, 3, 2, 1, 5, 1, 2, 2, -2, 2]
// 3 boomerangs in this sequence:  [3, 7, 3], [1, 5, 1], [2, -2, 2]
Be aware that boomerangs can overlap, like so:

[1, 7, 1, 7, 1, 7, 1]
// 5 boomerangs (from left to right): [1, 7, 1], [7, 1, 7], [1, 7, 1], [7, 1, 7], and [1, 7, 1]
Examples
countBoomerangs([9, 5, 9, 5, 1, 1, 1]) ➞ 2

countBoomerangs([5, 6, 6, 7, 6, 3, 9]) ➞ 1

countBoomerangs([4, 4, 4, 9, 9, 9, 9]) ➞ 0
Notes
[5, 5, 5] (triple identical digits) is NOT considered a boomerang because the middle digit is identical to the first and last. */

function countBoomerangs(arr) {
  let count = 0;
  for (let i = 0; i < arr.length - 2; i++) {
    if (isBoomerang(arr.slice(i, i + 3))) {
      count++;
    }
  }

  return count;
}

function isBoomerang(subArray) {
  
  if (subArray.length !== 3) {
    return false;
  }

  return subArray[0] === subArray[2] && subArray[0] !== subArray[1];
}

/* The .length property on an array will return the number of elements in the array. For example, the array below contains 2 elements:

[1, [2, 3]]
// 2 elements, number 1 and array [2, 3]
Suppose we instead wanted to know the total number of non-nested items in the nested array. In the above case, [1, [2, 3]] contains 3 non-nested items, 1, 2 and 3.

Write a function that returns the total number of non-nested items in a nested array.

Examples
getLength([1, [2, 3]]) ➞ 3

getLength([1, [2, [3, 4]]]) ➞ 4

getLength([1, [2, [3, [4, [5, 6]]]]]) ➞ 6

getLength([1, [2], 1, [2], 1]) ➞ 5
Notes
An empty array should return 0. */


function getLength(arr) {
  let count = 0;
  for (let i = 0; i < arr.length; i++) {
    if (Array.isArray(arr[i])) {
      count += getLength(arr[i]);
    } else {
      count++;
    }
  }
  return count;
}


/* Write a function that calculates overtime and pay associated with overtime.

Working 9 to 5: regular hours
After 5pm is overtime
Your function gets an array with 4 values:

Start of working day, in decimal format, (24-hour day notation)
End of working day. (Same format)
Hourly rate
Overtime multiplier
Your function should spit out:

$ + earned that day (rounded to the nearest hundreth)
Examples
overTime([9, 17, 30, 1.5]) ➞ "$240.00"

overTime([16, 18, 30, 1.8]) ➞ "$84.00"

overTime([13.25, 15, 30, 1.5]) ➞ "$52.50"
2nd example explained:

From 16 to 17 is regular, so 1 * 30 = 30
From 17 to 18 is overtime, so 1 * 30 * 1.8 = 54
30 + 54 = $84.00 */


function overTime(arr) {
  const startHour = arr[0];
  const endHour = arr[1];
  const hourlyRate = arr[2];
  const overtimeMultiplier = arr[3];
  
  let regularHoursPay = 0;
  let overtimePay = 0;

  if (endHour <= 17) {
    regularHoursPay = (endHour - startHour) * hourlyRate;
  } else if (startHour < 17) {
    regularHoursPay = (17 - startHour) * hourlyRate;
    overtimePay = (endHour - 17) * hourlyRate * overtimeMultiplier;
  } else {
    overtimePay = (endHour - startHour) * hourlyRate * overtimeMultiplier;
  }

  const totalPay = regularHoursPay + overtimePay;

  return "$" + totalPay.toFixed(2);
}